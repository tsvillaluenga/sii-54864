// Mundo.cpp: implementation of the CMundo class.
//Tomás Sánchez Villaluenga
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//Librerías para FIFO
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//Librería para el write
#include <unistd.h>

//Librería mmap
#include <sys/mman.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fd_FIFO); //Cierro file descriptor tuberia (dejor de utilizar tuberia)
	close(fd_FIFO_cliente);
	unlink("FIFO_cliente");
	close(fd_FIFO_teclas);
	unlink("FIFO_teclas");
	//munmap(p_datos_bot, sizeof(DatosMemCompartida)); //Elimino proyeccion en memoria
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	
	
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	
	typedef struct puntuacion{
		int jugador;
		int puntos;
	};
	puntuacion p;
	
	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		p.jugador = 2;
		p.puntos = puntos2;
		write(fd_FIFO, &p, sizeof(puntuacion));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		p.jugador = 1;
		p.puntos = puntos1;
		write(fd_FIFO, &p, sizeof(puntuacion));
	}


/*	//Actualización datos memoria compartida
	
	p_datos_bot->esfera = esfera;
	p_datos_bot->raqueta1 = jugador1;
	
	
	switch (p_datos_bot->accion){
		case 1: CMundo::OnKeyboardDown('w',0,0); break;
		case -1: CMundo::OnKeyboardDown('s',0,0); break;
	}
	
	
	//SELECCIÓN FIN DE JUEGO
	if(puntos1 == 5 || puntos2 == 5){
		p_datos_bot->cerrar = 1;
		destruir_mundo = 1;
	}
*/

	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", esfera.radio, esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2, jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	//printf("%s\n",cad);
	write(fd_FIFO_cliente, cad, sizeof(cad));	//Envío texto
	
}



void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	}
}



void CMundo::RecibeComandosJugador()
{
	//Abre el FIFO para leer las teclas
    if ((fd_FIFO_teclas=open("FIFO_teclas", O_RDONLY))<0) {
        perror("No puede abrirse el FIFO de las teclas");
    }
     while (1) {
            usleep(10);
            char cad[100];
            read(fd_FIFO_teclas, cad, sizeof(cad));	//Adquiero valor FIFO_teclas
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}


void* CMundo::hilo_comandos(void* d){
	CMundo* p=(CMundo*) d;
	p->RecibeComandosJugador();
}


void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
//Esfera
	esfera.setPos(0,0);
	esfera.setVel(2,2);
	



    //Abrimos la tubería FIFO
    if ((fd_FIFO=open("FIFO", O_WRONLY))<0) {
        perror("No puede abrirse el FIFO");
    }
    
/*   //Proyección de fichero en memoria para el bot
    if ((fd_mmap=open("mmap_bot", O_RDWR|O_CREAT|O_TRUNC, 0600))<0) {
        perror("No puede abrirse el fichero de proyección de memoria");
    }
    
    ftruncate(fd_mmap, sizeof(DatosMemCompartida));
		
    if ((void *)(p_datos_bot=(DatosMemCompartida *)(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0))) == (void *)MAP_FAILED) {
		perror("Error en la proyeccion del archivo");
		close(fd_mmap);
	}
	close(fd_mmap);
*/

 	//Abrimos la tubería FIFO para conectar con el cliente
	if ((fd_FIFO_cliente=open("FIFO_cliente", O_WRONLY))<0) {
        perror("No puede abrirse el FIFO del cliente");
    	}
        
    	pthread_create(&thid1, NULL, hilo_comandos, this);
    	
}
