// Esfera.cpp: implementation of the Esfera class.
//
//AUTOR: Tomás Sánchez Villaluenga
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}

void Esfera::setPos(float px, float py){
	centro.x = px;
	centro.y = py;
}

void Esfera::setVel(float vx, float vy){
	velocidad.x = vx;
	velocidad.y = vy;
}

void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro = centro + velocidad * t;		//SOBRECARGADO para -->  Vector2D * FLOAT    y NO para:  FLOAT * Vector2D
	newRadio(t);
	//velocidad = velocidad*(1+0.05*t);	//Si queremos aumentar la velocidad según avanza el juego
}

void Esfera::newRadio(float t){
	radio = radio*(1-0.2*t);
	if ( radio <0.2) radio = 1;
}


/*void Esfera::newRadio(float t){
int cont = 0;
	if (cont = 0) radio = radio*(1-0.2*t);
	if ( radio <= 0.2) {cont = 1; radio = radio*(1+6*t);}
	else if (cont = 1 && radio < 1.2) radio = radio*(1+6*t);
	else if (radio >= 1.2) { cont = 2; radio = radio*(1-0.2*t);}
	else if (cont = 2 && radio > 0.2) radio = radio*(1-0.2*t);
	
}*/
