
# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
 
## [1.4] - 2021-12-12
 
### Added
-  Client and server program to activate and broadcast the game

### Changed
-  The game don't finish at 5 points yet
 
### Fixed



## [1.3] - 2021-11-20
 
### Added
-  Logger program where we show the player's points and who match the point
-  Bot program that controle the left racket or both of them by default
-  Game finish when one of them match 5 points

### Changed
 - 
 
### Fixed


## [1.2] - 2021-11-11
 New use of the application.
 
### Added
-  New use of the objects of the applicacion. Sphere and racket movements added.
-  Sphere variable radio

### Changed
 - 
 
### Fixed



## [1.1.1] - 2021-10-24
 Ignoring changes.
 
### Added
-  .gitignore file

### Changed
 - "bin" directory was deleted
 
### Fixed




## [1.1.0] - 2021-10-16
 First version of the directory. Minimun changes.
 
### Added
-  Authorship badge in files
-  Executable

### Changed
 
### Fixed

  
  


