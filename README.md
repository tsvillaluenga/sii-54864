# README

## Funcionamiento del juego
El juego trata de emular un partido de tenis. Si la pelota contacta con una de las paredes (derecha o izquierda), el punto será para el jugador contrario a esta pared.

### Controles
La raqueta izquierda se puede mover con los botones W (hacia arriba) y S (hacia abajo), aunque es controlada por un bot automáticamente. La raqueta derecha se mueve con los botones O (hacia arriba) y L (hacia abajo). 

En el caso de que la raqueta derecha no produzca movimiento durante más de 10 segundos seguidos, esta raqueta tambien se comenzará a controlar automaticamente por un bot hasta que se detecte un nuevo movimiento por teclado.

Cuando la pelota toca una pared lateral vuelve a aparecer en el centro.
